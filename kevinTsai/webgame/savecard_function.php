<?php
require ('api/config.php');
//---方法：存放手牌資料---
function save_card ($id)
{
    global $p_card, $m_card;
    global $db_host, $db_name, $db_pass;
    //---建立玩家手牌陣列
    $pcard = [];
    //---建立莊家手牌陣列
    $mcard = [];
    //---轉換資料型態array->str
    foreach ($p_card as $p_id => $cards)
    {
        foreach ($cards as $c_id => $card)
        {
            $str_pcard[] = $card[0] . '-' . $card[1];
            $pcard = implode ($str_pcard, ",");
        }
    }
    //---轉換資料型態array->str
    foreach ($m_card as $m_id => $cards)
    {
        foreach ($cards as $c_id => $card)
        {
            $str_mcard[] = $card[0] . '-' . $card[1];
            $mcard = implode ($str_mcard, ",");
        }
    }

    //--pdo連線
    $pdo = new PDO ($db_host, $db_name, $db_pass);
    $pdo -> query ('SET NAME "utf8"');
    $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

    //---存放玩家手牌、id
    $sql_insert = "INSERT INTO gameroom_info (id, p_card, m_id, m_card)
                   VALUES ('$id', '$pcard', '99', '$mcard')";
    $sth = $pdo -> prepare ($sql_insert);
    $sth -> execute ();

}
//---方法：清除資料---
function clear_data ($data)
{
    global $id;
    global $db_host, $db_name, $db_pass;
    //---pdo連線
    $pdo = new PDO ($db_host, $db_name, $db_pass);
    $pdo -> query ('SET NAME "utf8"');
    $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);
    
    //---清除資料
    $sql_clear = "DELETE FROM gameroom_info WHERE id = '$id'";
    $sth = $pdo -> prepare ($sql_clear);
    $sth -> execute ();
}
//---方法：存放玩家戰績
function save_record ($p_result)
{
    global $id, $win, $lose, $tie;
    global $db_host, $db_name, $db_pass;
    if ($p_result == WIN)
    {
        $win += 1;
    }
    else if ($p_result == LOSE)
    {
        $lose += 1;
    }
    else if ($p_result == TIE)
    {
        $tie += 1;
    }
    
    //---將戰績存至DB---
    //---pdo連線
    $pdo = new PDO ($db_host, $db_name, $db_pass);
    $pdo -> query ('SET NAME "utf8"');
    $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

    //---查詢戰績
    $sql_saverecord = "UPDATE user_information SET win_frequency = '$win', 
                                                   lose_frequency = '$lose', 
                                                   tie_frequency = '$tie'
                       WHERE id = '$id'";
    $sth = $pdo -> prepare ($sql_saverecord);
    $sth -> execute ();
    
    return $p_result;
}
