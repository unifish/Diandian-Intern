<?php
//---建立物件:撲克牌---
class Card
{
    public $color;      //定義顏色
    public $point;      //定義點數

    public $color_name; //定義顏色名稱
    public $point_name; //定義點數名稱

    public $pic;        //定義圖片

    //---方法：牌的結構---
    public function __construct ($color, $point, $color_name, $point_name)
    {
        $this->color = $color;              //定義顏色
        $this->point = $point;              //定義點數

        $this->color_name = $color_name;    //定義顏色名稱
        $this->point_name = $poinr_name;    //定義點數名稱

        $this->detallpic ($color, $point);  //呼叫牌的內容
    }
    //---方法：牌的內容---
    private function detallpic ($color, $point)
    {
        //設定花色
        $color_map = array ('clubs', 'dumands', 'hearts', 'spades', );
        //設定點數
        $point_map = array (NULL, 'ace', '2', '3', '4', '5', '6', '7', 
                                  '8', '9', '10', 'jack', 'queen', 'king', );
        //尋找圖片檔案
        $this->$pic = $point_map[$point] . '_of_' . $color_map[$color];
    }
}

