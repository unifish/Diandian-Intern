<?php
session_start ();         //啟用session
include ('savecard_function.php');
include ('card_function.php');

//---判斷是否有登入---
if (true == $_SESSION['loginsuccess']) 
{
    //---輸入---
    $id = $_SESSION['id'];
    //$id = '1';

    //---建立牌組---
    //cards = [花色][點數]
    $cards = [];

    for ($c_color = 0; $c_color <= 3; $c_color++)
    {
        for ($c_point = 1; $c_point <=13; $c_point++)
        {
            $cards[] = array ($c_color, $c_point, );
        }
    }
    //---洗牌---
    $cards = shuffling ($cards);
    
    //---發牌---
    $p_card = [];   //建立玩家牌組
    $m_card = [];   //建立莊家牌組

    //---設定發牌張數(開場固定兩張)
    for ($c_number = 1; $c_number <= 2;$c_number++)
    {
        //---玩家人數(id數+1)
        for ($p_number = 1; $p_number <= count ($id) + 1; $p_number++)
        {
            if (0 == count ($cards))
            {
                echo "牌已發完!!!" . "\n";
                break 2;
            }
            //---輸入玩家牌組
            $p_card[$p_number][] = array_shift ($cards);
        }
    }
    //---輸入莊家牌組
    $m_card[] = array_shift ($p_card);
    //===存放資料===
    //---呼叫存放資料function
    $savecard = save_card ($id);

    //---跳轉到遊戲室
    header ('location:gameroom.html');
    $_SESSION['cards'] = $cards;
}
else
{
    //---跳轉到登入頁
    header ('location:login.html');
}

