<?php
require ('api/config.php');
//---方法：查詢莊家手牌---
function select_mcard ($id)
{
    global $db_host, $db_name, $db_pass;
    //---pdo連線
    $pdo = new PDO ($db_host, $db_name, $db_pass);
    $pdo -> query ('SET NAME "utf8"');
    $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

    //---查詢莊家id
    $sql_mcard = "SELECT m_card FROM gameroom_info WHERE (id = '$id')";
    $sth = $pdo -> prepare ($sql_mcard);
    $sth -> execute();
    $result = $sth -> fetch (PDO::FETCH_ASSOC);

    return $result;
}

//---方法：查詢玩家手牌---
function select_pcard ($id)
{
    global $db_host, $db_name, $db_pass;
    //---pdo連線
    $pdo = new PDO ($db_host, $db_name, $db_pass);
    $pdo -> query ('SET NAME "utf8"');
    $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

    //---查詢玩家id
    $sql_pcard = "SELECT p_card FROM gameroom_info WHERE (id = '$id')";
    $sth = $pdo -> prepare ($sql_pcard);
    $sth -> execute();
    $result = $sth -> fetch (PDO::FETCH_ASSOC);

    return $result;
}

//---方法：查詢玩家戰績
function select_record ($id)
{
    global $db_host, $db_name, $db_pass;
    //---pdo連線
    $pdo = new PDO ($db_host, $db_name, $db_pass);
    $pdo -> query ('SET NAME "utf8"');
    $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

    //---查詢戰績
    $sql_userrecord = "SELECT win_frequency, lose_frequency, tie_frequency
                       FROM `user_information` WHERE (`id` = '$id')";
    $sth = $pdo -> prepare ($sql_userrecord);
    $sth -> execute();
    $result = $sth -> fetch (PDO::FETCH_ASSOC);

    return $result;
}
