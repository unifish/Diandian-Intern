<?php
include ('card.php');

//---建立牌組---cards[花色][點數]
$cards = array ();

//---建立牌組---52張牌 cards[] = array (花色, 點數)
//---建立4種花色
for ($c_color = 0; $c_color <= 3; $c_color++)
{
    //---建立13種點數
    for ($c_point = 1; $c_point <= 13; $c_point++)
    {
        $cards[] = array ($c_color, $c_point,); 
    }
}

//---洗牌
$cards = shuffling ($cards);


//---輸入---
//---輸入錯誤判斷、顯示---
//---判斷輸入格式是否正確
if (1 < $argc and 3 > $argc)
{
    //格式正確，存入玩家人數
    if (is_numeric ($argv[1]))
    {
        $player_Quantity = $argv[1];  //玩家人數
        /*
        $card_Quantity = $argv[2];    //發排張數
        */
    }
    else 
    {
        echo "輸入錯誤，請輸入數字!!!" . "\n";
        exit;
    }
}
else
{
    echo "輸入錯誤，請重新輸入!!!" . "\n";
    echo "正確格式為：檔名 人數 " . "\n";
    exit;
}

//---洗牌---
$cards = shuffling ($cards);  //呼叫方法：洗牌

//列印牌
//print_r(show_cardface($cards));
//print_r($cards);

//---發牌---
//---建立玩家排組
$p_card = [];
//---設定發牌張數(開場固定發兩張)
for ($c_number = 1; $c_number <= 2; $c_number++)
{
    //---設定玩家人數，加一位莊家
    for ($p_number = 0; $p_number <= $player_Quantity; $p_number++)
    {
        //---當牌組為0時，跳出迴圈
        if (0 == count ($cards))
        {
            echo "牌已發完!!!" . "\n";
            break 2;
        }
        //---輸入玩家牌組
        $p_card[$p_number][] = array_shift ($cards);
    }
}
//print_r($p_card);
/*p_card => array (
             [1] => array (----->玩家
                      [0] => array(----->牌
                                  [0]--->花色
                                  [1]--->點數
                                  )
                          )
                  )
*/
//---補牌判斷---
foreach ($p_card as $player => $player_info)
{
    //---牌組為0跳出迴圈
    if (0 == count ($cards))
    {
        break;
    }
    $next_player = false;
    while (false == $next_player)
    {
        //---判斷是否需要補牌
        //---未達14點補牌
        if (14 > point_compute ($player))
        {
            //---補牌
            $cards_empty = add_card ($player);
            //---判斷發牌
            if (true == $cards_empty)
            {
                print_r ("牌已發完!!!\n");
                break 2;
            }
            //---判斷爆牌
            if (21 < point_compute ($player))
            {
                $next_player = true;
            }
            //---判斷過五關
            if (5 == count ($p_card[$player]))
            {
                $next_player = true;
            }
        }
        //---大於14小於18猶豫補牌
        else if (14 <= point_compute ($player) and 18 > point_compute ($player))
        {
            //---擲骰決定是否補牌
            $dice = rand (1, 6);
            //---手上有A直接補
            if (true == have_a ($player))
            {
                //---補牌
                $cards_empty = add_card ($player);
                //---判斷發牌
                if (true == $cards_empty)
                    {
                        print_r ("牌已發完!!!\n");
                        break 2;
                    }
                //---判斷爆牌
                if (21 < point_compute ($player))
                {
                    $next_player = true;
                }
                //---判斷過五關
                if (5 == count ($p_card[$player]))
                {
                    $next_player = true;
                }
            }
            //---沒有A，骰骰子決定
            //---擲到1、6就補牌
            else if (1 == $dice || 6 == $dice)
            {
                //---補牌
                $cards_empty = add_card ($player);
                if (true == $cards_empty)
                {
                    print_r ("牌已發完!!!\n");
                    break 2;
                }
                //---判斷是否爆牌
                if (21 < point_compute ($player))
                {
                    $next_player = true;
                }
                //---判斷是否過五關
                if (5 == count ($p_card[$player]))
                {
                    $next_player = true;
                }
            }
        }
        else
        {
            $next_player = true;
        }
    }
}
//print_r($p_card);

//---玩家分數存放---
/*player_score = array (
                   [0] => array (
                            [0] => '總點數'
                            [1] => '手牌張數'
                            [2] => '狀態'
                                )
                       )
*/
$player_score = [];

//---大於21爆牌
foreach ($p_card as $player => $player_info)
{ 
    $score_point = point_compute ($player);                                //總點數
    $score_cards = count ($player_info);                                        //手牌張數
    $score_judge = (21 < $score_point) ? "爆牌" : "";                      //爆牌判斷
    $player_score[] = array ($score_point, $score_cards, $score_judge, );  //---存入玩家分數
}
//print_r($player_score);
//print_r($p_card);

//---建立莊家排組---
/*maker_score = array (
                  [0] => '總點數'
                  [1] => '手牌張數'
                  [2] => '狀態'
                      )
*/
$maker_score = [];
$maker_score = array_pop($player_score);  //莊家排組取出
$maker_point = $maker_score[0];           //莊家點數
$maker_card = $maker_score[1];            //莊家手牌張數
//print_r($maker_score);

//---定義勝敗常數---
define ('WIN', " 你贏了！！");  
define ('LOSE', " 你輸了 orz");
define ('TIE', " 平手~~~");
//--判斷玩家與莊家勝敗---
foreach ($player_score as $player => $player_info)
{
    //---當莊家爆牌
    if (21 < $maker_point)
    {
        if ($player_info[2] == "")            //玩家沒爆 -->WIN
        {
            $player_score[$player][2] = WIN;
        }
        else                                  //玩家爆牌 -->LOSE
        {
            $player_score[$player][2] = " 爆牌！" . LOSE ;
        }
    }
    //---當莊家過五關
    else if (5 == $maker_card)
    {
        //---玩家過五關且沒爆牌
        if (5 == $player_info[1] and "" == $player_info[2])
        {
            if ($maker_point > $player_info[0])      //莊家>玩家 -->LOSE
            {
                $player_score[$player][2] = LOSE;
            }
            else if ($maker_point < $player_info[0]) //玩家>莊家 -->WIN
            {
                $player_score[$player][2] = " 過五關！！！" . WIN;
            }
            else                                     //點數相同 -->TIE
            {
                $player_score[$player][2] = TIE;
            }
        }
        //---玩家過五關但爆牌 -->LOSE
        else if (5 == $player_info[1] and "" != $player_info[2])
        {
            $player_score[$player][2] = " 爆牌" . LOSE;
        }
        //---玩家沒過五關 -->LOSE
        else if (5 != $player_info[1])
        {
           $player_score[$player][2] = LOSE; 
        }
    }
    //---當玩家過五關而莊家沒過 -->WIN
    else if (5 != $maker_card and 5 == $player_info[1])
    {
        if ("" != $player_info[2])
        {
            $player_score[$player][2] = " 爆牌" . LOSE;
        }
        else
        {
            $player_score[$player][2] = " 過五關！！！" . WIN;
        }
    }
    //---當莊家<=21點
    else if (21 >= $maker_point)
    {
        //---玩家>莊家
        if ($player_info[0] > $maker_point)
        {
            if ("" != $player_info[2])           //玩家爆牌 -->LOSE
            {
                $player_score[$player][2] = " 爆牌" . LOSE;
            }
            else                                 //玩家沒爆 -->WIN
            {
            $player_score[$player][2] = WIN;
            }
        }
        //---玩家<莊家 -->LOSE
        else if ($player_info[0] < $maker_point)
        {
            $player_score[$player][2] = LOSE;
        }
        //---玩家=莊家 -->TIE
        else
        {
            $player_score[$player][2] = TIE;
        }
    }
}
//print_r($player_score);
//print_r($p_card);
$player_score[] = $maker_score;  //---莊家牌組放回最後
//print_r($player_score);

//---輸出---
//---設定顯示玩家牌花色點數
foreach ($p_card as $key => $data)
{
    $get_face = show_cardface ($data);     //呼叫顯示牌面方法
    $out_card = implode (" ", $get_face);  //轉換陣列輸出
    $player_sum = $player_score;           //玩家結果
    $maker_sum = $maker_score;             //莊家結果
    
    //輸出結果
    if ($key != $player_Quantity )
    {
        print_r ("玩家" . strval ($key + 1) . "：" . $out_card . 
                 " 總共是：" . $player_sum[$key][0] . $player_sum[$key][2] . "\n");
    }
    else if ($key == $player_Quantity) 
    print_r ("莊家：" . $out_card . 
             " 總共是：" . $maker_sum[0] . $maker_sum[2] . "\n");
}

//---方法：洗牌---
function shuffling ($card)
{
    //將牌隨機分成三份
    do
    {
        $box[rand(0, 2)][] = array_shift ($card);
    } while (count ($card) != 0);

    //每份洗牌
    shuffle ($box[0]);
    shuffle ($box[1]);
    shuffle ($box[2]);

    //一張一張放回
    do
    {
        $pokers[] = array_shift ($box[0]);
    } while (count ($box[0]) != 0);

    do
    {
        $pokers[] = array_shift ($box[1]);
    } while (count ($box[1]) != 0);

    do
    {
        $pokers[] = array_shift ($box[2]);
    } while (count ($box[2]) != 0);

    return $pokers;
}

//---方法：定義牌花色、點數---
function show_cardface ($get_cardface)
{
    //定義花色
    $color = array ('梅花', '方塊', '紅心', '黑桃', );
    //定義點數
    $point = array (NULL, 'A', '2', '3', '4', '5', '6', 
                    '7', '8', '9', '10', 'J', 'Q', 'K', );
    //建立牌面陣列
    $showface = [];
    foreach ($get_cardface as $card => $cardface)
    {
        $showface[] =  $color[$cardface[0]] . //牌的花色
                       $point[$cardface[1]];  //牌的點數 
    }
    return $showface;
}

//---方法：補牌---
function add_card ($add_card)
{
    global $cards, $p_card;   
    if (0 != count ($cards))
    {
        $take_card = array_shift ($cards);  //取出牌組第一張
        $p_card[$add_card][] = $take_card;  //加進玩家牌組
        return false;
    } 
    else 
    {
        return true;
    }
}

//---方法：計算點數---
function point_compute ($get_pointsum)
{
    global $p_card;
    $sum = 0;
    $have_A = 0;
    foreach ($p_card[$get_pointsum] as $card => $cardpoint)
    {
        $get_point = $cardpoint[1];
        //---點數為11、12、13時，視為10點
        if (11 == $get_point or 12 == $get_point or 13 == $get_point)
        {
            $sum += 10;
        }
        else if (1 == $get_point)
        {
            $have_A += 1;
        }
        else
        {
            $sum += $get_point;
        }
    }
    //---A視為1或11
    for ($i = 0; $i < $have_A; $i++)
    {
        $A_point = (($sum + 11) > 21) ? 1 : 11;
        $sum += $A_point;
    }
    return $sum;
}
//---方法：手上是否有A---
function have_A ($get_player)
{
    global $p_card;
    $have_A = false;
    foreach ($p_card[$get_player] as $card => $cardpoint)
    {
        $get_point = $cardpoint[1];
        if (1 == $get_point)
        {
            $have_A = true;
        }
    }
    return $have_A;
}

