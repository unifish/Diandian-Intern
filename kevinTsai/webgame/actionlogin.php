<?php
session_start ();             //啟用session
require ('api/config.php');   //連接伺服器設定檔
get_magic_quotes_gpc ();      //啟用辨識特殊字元

//===判斷特殊字元===
if (!get_magic_quotes_gpc ())
{
    $account = addslashes ($_POST['acc']);
    $password = addslashes ($_POST['pass']);
}
else
{
    $account = $_POST['acc'];   //使用者名稱
    $password = $_POST['pass'];   //使用者密碼
}

//---判斷名稱或密碼是否空值---
if ('' == $account || '' == $password)
{
    //---空值跳回登入頁
    header ("location:login.html");
}
else
{
    //---pdo連接
    $pdo = new PDO ($db_host, $db_user, $db_pass);
    $pdo -> query ('SET NAMES"utf8"');
    $sql = "SELECT * FROM `user_information` WHERE (`account` = '$account' AND `password` = '$password')";

    $sth = $pdo -> prepare ($sql);
    $sth -> execute (array ($account, $password));
    $result = $sth -> fetch (PDO::FETCH_ASSOC);
    //---查詢成功，跳轉至遊戲大廳
    if ($result)
    {
        $_SESSION['loginsuccess'] = true;
        $_SESSION['id'] = $result['id'];
        header ("location:gamelobby.html");
    }
    else
    {
        $_SESSION['loginsuccess'] = false;
        header ("location:login.html");
    }
    $pdo = NULL;
}


