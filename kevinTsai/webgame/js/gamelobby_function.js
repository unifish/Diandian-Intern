var p_id;

function update_page(get_id) {
    p_id = get_id;
    var URLs_username="/webgame/api/get_playername.php";
    var URLs_userinfo="/webgame/api/get_playerrecord.php";
    console.log(get_id);
    $.ajax({
        url: URLs_username,
        data: "id=" + get_id,
        type: "POST",
        dataType: "text",

        success: function(msg){
            console.log(msg);
            var name_result = JSON.parse(msg);
            if (1 == name_result.status) {
                document.getElementById("user_mane").innerHTML="名稱：" + name_result.user_name;
            } else if (-87 == name_result.status) {
                console.log("ERROR-87" + name_result);
            } else {
                console.log("ERROR-OTHER" + name_result);
            }
        },
        error:function(xhr, ajaxOptions, thrownError){
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });

    $.ajax({
        url: URLs_userinfo,
        data: "id=" + get_id,
        type: "POST",
        dataType: "text",

        success: function(msg){
            console.log(msg);
            var info_result = JSON.parse(msg);
            if (1 == info_result.status) {
                document.getElementById("user_win").innerHTML="勝場：" + info_result.win_frequency + "場";
                document.getElementById("user_lose").innerHTML="敗場：" + info_result.lose_frequency + "場";
                document.getElementById("user_tie").innerHTML="平手：" + info_result.tie_frequency + "場";
                var sum_score = info_result.win_frequency + info_result.lose_frequency + info_result.tie_frequency;
                if (0 != sum_score) {
                    var winning = (info_result.win_frequency / sum_score) * 100;
                    var formate_winning = FormatNumber(winning, 2);
                } else {
                    var formate_winning = "0.00";
                }
                document.getElementById("user_winning").innerHTML="勝率：" + formate_winning + "％";
            } else if (-87 == info_result.status) {
                console.log("ERROR-87" + info_result);
            } else {
                console.log("ERROR-OTHER" + info_result);
            }
        },
        error:function(xhr, ajaxOptions, thrownError){
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
}

//登出遊戲
function logout() {
    var id = p_id;
    var URLs="/webgame/api/set_logout.php";
    $.ajax({
        url: URLs,
        data: "id=" + id,
        type: "POST",
        dataType: "json",

        success: function(msg){
            console.log(msg);
        },
        error:function(xhr, ajaxOptions, thrownError){
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
    document.location.href="/webgame/index.html";
}

    function FormatNumber(num,decimal){
//=========================================================================='
//功能說明:傳入數字，及小數位數，傳回格式化後數字字串                       '
//傳入參數:                                                                 '
//          num     :傳入的數字。                                           '
//          decimal :保留小數位數。                                         '
//傳回參數:Error   : Return false                                           '
//         Success : Return 格式化後數字字串                                '
//=========================================================================='
    var tmpNumber1=num.toString();//轉成文字型態
    var numlen=tmpNumber1.length;//取得字數
    var decimalIdx=tmpNumber1.indexOf('.');//取得小數點所在位置
    var Intlen;
    var decimallen;
    if(decimalIdx!=-1)
        decimallen=numlen-decimalIdx-1;//依據小數點位置，算出整數長度
    else
        decimalIdx=0;
    var tmpNumber2; 
    if(decimal!=0)
    {
        tmpNumber2 = num*(Math.pow(10,decimal));
        tmpNumber2=Math.round(tmpNumber2)/(Math.pow(10,decimal));
    }
    else
        tmpNumber2 = tmpNumber1 + '.0';
    //四捨五入到指定位置結束
    
//  window.alert('四捨五入後的數字：' + tmpNumber2);
    var decimalNum='';//宣告小數點後文字
    if(tmpNumber1.indexOf('.')!=-1)
        decimalNum = tmpNumber2.toString().substring(tmpNumber2.toString().indexOf('.')+1,tmpNumber2.toString().indexOf('.')+decimal+1);//取得小數點位數
//  window.alert('小數點文字：' + decimalNum);
    var rtndecimal=decimalNum;
    for(j=0;j<(decimal-decimalNum.length);j++)
        rtndecimal = rtndecimal + '0';
//  window.alert('補0後的小數後文字:' + rtndecimal);
    var IntNum;//宣告整數文字
//  window.alert('小數點位置：' + tmpNumber2.toString().indexOf('.'));
    //取得整數文字
    IntNum=tmpNumber2.toString().substring(tmpNumber2.toString().indexOf('.'),0);
    if(tmpNumber2.toString().indexOf('.')==-1)
        IntNum = tmpNumber2.toString();
    if(tmpNumber2.toString().indexOf('.')==0)
        IntNum = '0';   
//  window.alert('整數文字：' + IntNum);
    var lpcnt = Math.floor(IntNum.length/3);
    if(IntNum.substring(0,1)=='-')
        lpcnt--;
//  window.alert(lpcnt);
    var tmpNumber3='';
    for(i=0;i<lpcnt;i++)
    {
        tmpNumber3=',' + IntNum.substring(IntNum.length,IntNum.length-3).toString() + tmpNumber3;
        IntNum=IntNum.substring(IntNum.length-3,0);
    }
//  window.alert('格式化後的整數：' + tmpNumber3)
    tmpNumber3 = IntNum + tmpNumber3;
    if(tmpNumber3.substring(0,1)==',')
        tmpNumber3 = tmpNumber3.substring(1,tmpNumber3.length);
    return tmpNumber3 + '.' + rtndecimal;   
}