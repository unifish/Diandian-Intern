
var beforeUrl = "/webgame/api/";
var p_id;

var m = new Array();
m[0] = document.getElementById("m1");
m[1] = document.getElementById("m2");
m[2] = document.getElementById("m3");
m[3] = document.getElementById("m4");
m[4] = document.getElementById("m5");

var p = new Array();
p[0] = document.getElementById("p1");
p[1] = document.getElementById("p2");
p[2] = document.getElementById("p3");
p[3] = document.getElementById("p4");
p[4] = document.getElementById("p5");

var maker_board = document.getElementById("maker_score");
var player_board = document.getElementById("player_score");

var res_pic = document.getElementById("result_pic");

var color = ['clubs', 'diamonds', 'hearts', 'spades', ];
var point = [null, 'ace', '2', '3', '4', '5', '6', '7',
            '8', '9', '10', 'jack', 'queen', 'king', ];

//讀取資料
function loadinginfo(id) {
p_id = id;
get_result(id);
clear_table(id);
}

//更新頁面
function get_result(id) {
    var URLs= beforeUrl + "get_gameresult.php";
    $.ajax({
        url: URLs,
        data:"id=" + id,
        type:"POST",
        dataType: "json",

        success: function(msg) {
            console.log(msg);
            //show手牌
            for (var i = 0; i < msg.m_card.length; i++) {
                poke = msg.m_card[i];
                get_src = get_pokepic(poke);
                m[i].style = "display:inline-block";
                m[i].src = get_src;
            }
            for (var i = 0; i < msg.p_card.length; i++) {
                poke = msg.p_card[i];
                get_src = get_pokepic(poke);
                p[i].style = "display:inline-block";
                p[i].src = get_src;
            }
            //show點數
            maker_text = "莊家：" + msg.m_point + "點";
            maker_board.innerHTML = maker_text;

            player_text = "玩家：" + msg.p_point + "點" + msg.gameresult;
            player_board.innerHTML = player_text;
            //show圖片
            if ("WIN" == msg.p_result) {
                res_pic.src = "images/web/res_win.png"
            } else if ("TIE" == msg.p_result) {
                res_pic.src = "images/web/res_tie.png"
            } else if ("LOSE" == msg.p_result) {
                res_pic.src = "images/web/res_lose.png"
            } else {
                res_pic.src = "images/web/playerpis.png"
            }

        },
        error:function(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
}

//繼續遊戲
function contunie() {
    document.location.href="/webgame/gamestart.php";
}

//返回大廳
function gotolobby() {
    document.location.href="/webgame/gamelobby.html";
}

//將數值轉網址
function get_pokepic(str) {
    var card = str.split("-");
    if (99 == card[0]) {
        var showcolor = "cards";
    } else {
        var showcolor = color[card[0]];
    }

    if (99 == card[1]) {
        var showpoint = "back";
    } else {
        var showpoint = point[card[1]];
    }
    var url = "images/poker_cards/" + showpoint + "_of_" + showcolor + ".png";
    return url;
}

//呼叫清除資料
function clear_table(id) {
    var URLs = beforeUrl + "reset_gamedata.php";
    $.ajax({
        url: URLs,
        data:"id=" + id,
        type:"POST",
        success: function(mmsg) {
            console.log(mmsg);
        },
        error:function(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
}