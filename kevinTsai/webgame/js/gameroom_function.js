//設定變數
var beforeUrl = "/webgame/api/";
var p_id;

var m = new Array();
m[0] = document.getElementById("m1");
m[1] = document.getElementById("m2");
m[2] = document.getElementById("m3");
m[3] = document.getElementById("m4");
m[4] = document.getElementById("m5");

var p = new Array();
p[0] = document.getElementById("p1");
p[1] = document.getElementById("p2");
p[2] = document.getElementById("p3");
p[3] = document.getElementById("p4");
p[4] = document.getElementById("p5");

var room_btn1 = document.getElementById("btn1");
var room_btn2 = document.getElementById("btn2");
var room_btn3 = document.getElementById("btn3");

var text_board = document.getElementById("show_text");

var maker_p = document.getElementById("maker_p");
var player_p = document.getElementById("player_p");

var color = ['clubs', 'diamonds', 'hearts', 'spades', ];
var point = [null, 'ace', '2', '3', '4', '5', '6', '7',
            '8', '9', '10', 'jack', 'queen', 'king', ];

//開始遊戲
function startgame(id) {
    p_id = id;
    //顯示莊家手牌
    maker_refresh(p_id);
    //顯示玩家手牌
    refresh_player(p_id);
    //BONUS
    var get_bonus = (Math.random() * 100);
    console.log(get_bonus);
    if (20 > get_bonus) {
        bonuspic();
    }

    text_board.innerHTML = "開始遊戲，是否要抽牌呢？";
}

//刷新莊家桌面
function maker_refresh(id) {
    var URLs= beforeUrl + "get_makercard.php";
    $.ajax({
        url: URLs,
        data:"id=" + id,
        type:"POST",

        success: function(msg) {
            console.log(msg);
            for (var i = 0; i < msg.m_card[0].length; i++) {
                poke = msg.m_card[0][i];
                get_src = get_pokepic(poke);
                m[i].style = "display:inline-block";
                m[i].src = get_src;
            }
        },
        error:function(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
}

//刷新玩家桌面
function refresh_player(id) {
    var URLs = beforeUrl + "get_playercard.php";
    $.ajax({
        url: URLs,
        data:"id=" + id,
        type:"POST",

        success: function(msg) {
            console.log(msg);
            for (var i = 0; i < msg.p_card[0].length; i++) {
                poke = msg.p_card[0][i];
                get_src = get_pokepic(poke);
                p[i].style = "display:inline-block";
                p[i].src = get_src;
            }
        },
        error:function(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
}

//補牌判斷
function get_card(select) {
    if (true == select) {
        var URLs = beforeUrl + "get_card.php";
        $.ajax({
            url: URLs,
            data:"id=" + p_id + "&takecard=true",
            type:"POST",

            success: function(gmsg) {
                console.log(gmsg);
                if (21 == gmsg.status) {
                    close_buttn();
                    text_board.innerHTML = "抱歉爆牌囉～真可惜啊";
                    goto_result();
                } else if (5 == gmsg.status) {
                    close_buttn();
                    text_board.innerHTML = "過五關阿！？好棒棒啊。來看莊家要怎辦囉～";
                    setTimeout("call_makeract(p_id)",3000);
                } else {
                    text_board.innerHTML = "已經抽牌了，是否要繼續抽？";
                }
                refresh_player(p_id);
            },
            error:function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(ajaxOptions + " or " + thrownError);
            }
        });
    } else if (false == select) {
        close_buttn();
        text_board.innerHTML = "就這樣囉？那～輪到莊家";
        setTimeout("call_makeract(p_id)",3000);
    }
}

//關閉按鈕
function close_buttn() {
    room_btn1.disabled = "disabled";
    room_btn1.onclick = "";
    room_btn2.disabled = "disabled";
    room_btn2.onclick = "";
    room_btn3.disabled = "disabled";
    room_btn3.onclick = "";
}

//BONUS
function bonuspic() {
    maker_p.style = "display:inline-block";
    player_p.style = "display:inline-block";
}

//呼叫莊家行動
function call_makeract(id) {
    var URLs = beforeUrl + "set_makeraction.php";
    $.ajax({
        url: URLs,
        data:"id=" + id,
        type:"POST",
        success: function(mmsg) {
            console.log(mmsg);
            for (var i = 0; i < mmsg.m_card.length; i++) {
                poke = mmsg.m_card[i];
                get_src = get_pokepic(poke);
                m[i].style = "display:inline-block";
                m[i].src = get_src;
            }
        },
        error:function(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
    text_board.innerHTML = "遊戲結束，三秒後轉到結果頁";
    goto_result();
}

//轉跳結果頁
function goto_result() {
    setTimeout("location.href='gameresult.php'",3000);
}

//將數值轉網址
function get_pokepic(str) {
    var card = str.split("-");
    if (99 == card[0]) {
        var showcolor = "cards";
    } else {
        var showcolor = color[card[0]];
    }

    if (99 == card[1]) {
        var showpoint = "back";
    } else {
        var showpoint = point[card[1]];
    }
    var url = "images/poker_cards/" + showpoint + "_of_" + showcolor + ".png";
    return url;
}

//離開
function jumptolobby() {
    clear_table(p_id);
    document.location.href="gamelobby.html";
}

//呼叫清除資料
function clear_table(id) {
    var URLs = beforeUrl + "reset_gamedata.php";
    $.ajax({
        url: URLs,
        data:"id=" + id,
        type:"POST",
        success: function(mmsg) {
            console.log(mmsg);
        },
        error:function(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(ajaxOptions + " or " + thrownError);
        }
    });
}