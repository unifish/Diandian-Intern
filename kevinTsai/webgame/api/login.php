<?php
session_start ();             //啟用session
require ('config.php');   //連接伺服器設定檔
get_magic_quotes_gpc ();      //啟用辨識特殊字元

//===判斷特殊字元===
if (!get_magic_quotes_gpc ())
{
    $account = addslashes ($_POST['account']);
    $password = addslashes ($_POST['password']);
}
else
{
    $account = $_POST['account'];   //使用者名稱
    $password = $_POST['password'];   //使用者密碼 
}

//---判斷名稱或密碼是否空值---
if ('' == $account || '' == $password)
{
    //---空值傳送錯誤碼
    $status = -33;
    echo json_encode (array ('status' => $status));
}
else
{
    //---pdo連接
    $pdo = new PDO ($db_host, $db_user, $db_pass);
    $pdo -> query ('SET NAMES"utf8"');
    $sql = "SELECT * FROM `user_information` WHERE (`account` = '$account' AND `password` = '$password')";

    $sth = $pdo -> prepare ($sql);
    $sth -> execute (array ($account, $password));
    $result = $sth -> fetch (PDO::FETCH_ASSOC);
//---查詢成功回傳ID
if ($result)
    {
        $status = 1;
        $_SESSION['loginsuccess'] = true;
        $id = $result['id'];
        $_SESSION['id'] = $id;
        echo json_encode (array ('status' => $status, 'id' => $id));
    }
    else
    {
        $status = -87;
        $_SESSION['loginsuccess'] = false;
        echo json_encode (array ('status' => $status));
    }
    $pdo = NULL;
}
