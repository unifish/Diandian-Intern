<?php
require ('config.php');
include ('../search_function.php');

$id = $_POST['id'];
//$id = '4';
//---判斷是否為數字---
if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    //---呼叫查詢莊家手牌function
    $result = select_mcard ($id);

    //---查詢成功，回傳牌代號
    if ($result)
    {
        $status = 1;
        //---轉換資料型態str->array
        $mcard[] = explode (",", $result['m_card']);
        //---將第一張牌改傳牌背代號
        $mcard[0][0] = '99-99';

        echo json_encode (array ('status' => $status, 'm_card' => $mcard));
    }
    else
    {
        $status = -87;
        echo json_encode (array ('status' => $status));
    }
}
