<?php 
session_start ();

$id = $_POST['id'];


if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    $status = 1;

    $p_point = $_SESSION['p_point'];
    $m_point = $_SESSION['m_point'];
    $p_card = $_SESSION['p_card'];
    $m_card = $_SESSION['m_card'];
    $p_result = $_SESSION['p_result'];
    $gameresult = $_SESSION['gameresult'];

    echo json_encode (array ('status' => $status, 'p_point' => $p_point, 'm_point' => $m_point, 
                             'p_card' => $p_card, 'm_card' => $m_card, 'p_result' => $p_result,
                             'gameresult' => $gameresult));
}

