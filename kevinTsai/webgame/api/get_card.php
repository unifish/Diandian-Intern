<?php
session_start ();
require ('config.php');
include ('../card_function.php');
include ('../search_function.php');

$id = $_POST['id'];
//$id = '1';
$takecard = $_POST['takecard'];
//$takecard = true;
$cards = $_SESSION['cards'];

//---判斷是否為數字---
if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    //---判斷是否補排---
    //---不補排
    if (false == $takecard)
    {
        $status = 0;
        echo json_encode (array ('status' => $status));
    }
    else
    {
        //---呼叫查詢玩家手牌function
        $result = select_pcard ($id);

        //---查詢成功，進行補牌
        if ($result)
        {
            $status = 1;
            //---呼叫補牌function
            $take_card = add_card ();
            $num_card = $take_card[0] . '-' . $take_card[1];
            $new_result = $result['p_card'] . "," . $num_card;
            //---轉換資料型態str->array
            $pcard[] = explode (",", $new_result);

            //---呼叫檢查手牌function
            $status = check_playercard ($new_result);
            //---傳回補牌完的值
            echo json_encode (array ('status' => $status, 'p_card' => $pcard));

            //--將補牌完的值存回DB---
            //---pdo連線
            $pdo = new PDO ($db_host, $db_name, $db_pass);
            $pdo -> query ('SET NAME "utf8"');
            $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

            //---存放補完後的牌
            $sql_save = "UPDATE gameroom_info SET p_card = '$new_result'
                         WHERE id = '$id'";
            $sth = $pdo -> prepare ($sql_save);
            $sth -> execute ();

            $_SESSION['cards'] = $cards;
        }
        else
        {
            $status = -87;
            echo json_encode (array ('status' => $status));
        }
    }
}
