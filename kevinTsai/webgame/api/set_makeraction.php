<?php 
session_start ();
require ('config.php');
include ('../search_function.php');
include ('../card_function.php');

//---判斷是否為數字---
$id = $_POST['id'];
//$id = '1';
$cards = $_SESSION['cards'];

if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{    
    //---呼叫查詢莊家手牌function
    $result = select_mcard ($id);
    //---查詢成功，回傳牌代號
    if ($result)
    {
        $status = 1;
        //---轉換資料型態 str->array
        $mcard_array = explode (",", $result['m_card']);
        foreach ($mcard_array as $key => $info)
        {
            $m_card[] = explode ("-", $info );
        }
        //---判斷點數與牌數
        while (5 > count ($m_card))
        {
            if (17 > point_compute ($m_card))
            {
                $m_card[] = add_card ($m_card);
            }
            else
            {
                break ;
            }
            
        }
        //---轉換牌資料型態
        foreach ($m_card as $key => $cards)
        {
            $num_card[] = $cards[0] . '-' . $cards[1];
            $mcard = implode ($num_card, ",");
        }
        echo json_encode (array ('status' => $status, 'm_card' => $num_card));

        //---將補完牌的值傳回DB---
        //===pdo連線
        $pdo = new PDO ($db_host, $db_name, $db_pass);
        $pdo -> query ('SET NAME "utf8"');
        $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

        //---存放補完牌後的值
        $sql_save = "UPDATE gameroom_info SET m_card = '$mcard'
                     WHERE id = '$id'";
        $sth = $pdo -> prepare ($sql_save);
        $sth -> execute ();

        $_SESSION['cards'] = $cards;
    }
    else
    {
        $status = -87;
        echo json_encode (array ('status' => $status));
    }
}
