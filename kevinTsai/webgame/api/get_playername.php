<?php
require ('config.php');

$id = $_POST['id'];
//$id = '1';
//---判斷id是否為數字---
if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    //---pdo連線
    $pdo = new PDO ($db_host, $db_name, $db_pass);
    $pdo -> query ('SET NAME"utf8"');
    $pdo -> setAttribute (PDO::ATTR_EMULATE_PREPARES, false);

    //---查詢user_name
    $sql_username = "SELECT user_name FROM `user_information` WHERE (`id` = '$id')";
    $sth = $pdo -> prepare ($sql_username);
    $sth -> execute();
    $result = $sth -> fetch (PDO::FETCH_ASSOC);

    //---查詢成功，回傳user_name
    if ($result)
    {
        $status = 1;

        echo json_encode (array ('status' => $status, 'user_name' => $result['user_name']));
    }
    else
    {
        $status = -87;
        echo json_encode (array ('status' => $status));
    }
}
