<?php
include ('../savecard_function.php');

$id = $_POST['id'];
//---判斷是否為數字---
if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    $status = 1;
    //---呼叫清除資料function
    $result = clear_data($id);
    echo json_encode (array ('status' => $status));
}
