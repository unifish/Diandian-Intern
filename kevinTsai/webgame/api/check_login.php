<?php 
session_start ();
require ('config.php');
//---檢查session是否登入---
if (!isset ($_SESSION['loginsuccess']))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else 
{
    $status = 1;
    echo json_encode (array ('status' => $status, 'id' => $_SESSION['id']));
}
