<?php
require ('config.php');
include ('../search_function.php');

$id = $_POST['id'];
//$id = '1';
//---判斷id是否為數字
if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    $result = select_record ($id);
    
    //---查詢成功，回傳戰績資料
    if ($result)
    {
        $status = 1;
        echo json_encode (array ('status' => $status, 'win_frequency' => $result['win_frequency'],
                                 'lose_frequency' => $result['lose_frequency'], 
                                 'tie_frequency' => $result['tie_frequency']));
    }
    else
    {
        $status = -87; 
        echo json_encode (array ('status' => $status));
    }
}
