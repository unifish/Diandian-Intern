<?php
session_start ();
$id = $_POST['id'];

if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    $status = 1;
    session_destroy ();
    echo json_encode (array ('status' => $status));
}
