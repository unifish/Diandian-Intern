<?php
session_start ();
include ('../search_function.php');
include ('../savecard_function.php');
include ('../card_function.php');

//---輸入---i
$id = $_POST['id'];
//$id = '1';
if (!is_numeric ($id)) 
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    $status = 1;
    //---查詢手牌---
    //---呼叫查詢玩家手牌function
    //---呼叫查詢莊家手牌function
    $pcard = select_mcard ($id);
    $mcard = select_pcard ($id);
    //---轉換資料型態 str->array
    $pcard_array = explode (",", $mcard['p_card']);
    $mcard_array = explode (",", $pcard['m_card']);

    foreach ($pcard_array as $key => $info)
    {
        $p_card[] = explode ("-", $info );
    }
    foreach ($mcard_array as $key => $info)
    {
        $m_card[] = explode ("-", $info);
    }

    //---計算點數---
    //---呼叫計算點數function
    $p_point = point_compute ($p_card);
    $m_point = point_compute ($m_card);

    //---判斷爆牌---
    $p_judge = overtake21 ($p_point);
    $m_judge = overtake21 ($m_point);

    //---判斷勝敗---
    //---莊家爆牌
    if (21 < $m_point)
    {
        if ('' == $p_judge)   //玩家沒爆->WIN
        {
            if (5 == count ($pcard_array))
            {
                $p_result = WIN;
                $gameresult = " 過五關！";
            }
            else
            {
                $p_result = WIN;
                $gameresult = " 莊家爆牌！";
            }
        }
        else                  //玩家爆牌->LOSE
        {
            $p_result = LOSE;
            $gameresult = " 爆牌！";
        }
    }
    //---莊家過五關
    else if (5 == count ($mcard_array))
    {
        //---玩家過五關且沒爆牌
        if (5 == count ($pcard_array) && '' == $p_judge)
        {
            //---呼叫判斷大小function
            $p_result = compare ($m_point, $p_point);
            $gameresult = " 莊家玩家都過五關！！！" . $p_result;
        }
        //---玩家過五關但爆牌 -->LOSE
        else if (5 == count ($pcard_array) and "" != $p_judge)
        {
            $p_result = LOSE;
            $gameresult = " 爆牌！";
        }
        //---玩家沒過五關 -->LOSE
        else if (5 != count ($pcard_array))
        {   
            $p_result = LOSE;
            $gameresult = " 莊家過五關！";
        }
    }
    //---莊家沒爆
    else if (21 >= $m_point)
    {
        //---玩家過五關->WIN
        if (5 == count ($pcard_array) && '' == $p_judge)
        {
            $p_result = WIN;
            $gameresult = " 過五關！";
        }
        //---呼叫判斷大小function
        else if ('' == $p_judge)
        {
            $p_result = compare ($m_point, $p_point);
            $gameresult = compareToString ($p_result);
        }
        //玩家爆牌->LOSE
        else if ('' != $p_judge)
        {
            $p_result = LOSE;
            $gameresult = " 爆牌！";
        }
    }

    //---存入玩家戰績---
    //---呼叫查詢玩家戰績function
    $record = select_record ($id);
    //---判斷遊戲結果存入戰績
    $win = $record['win_frequency'];
    $lose = $record['lose_frequency'];
    $tie = $record['tie_frequency'];

    //--呼叫存放戰績function
    $save_record = save_record ($p_result);

    //---存放遊戲結果---
    $_SESSION['p_point'] = $p_point;
    $_SESSION['m_point'] = $m_point;
    $_SESSION['p_card'] = $pcard_array;
    $_SESSION['m_card'] = $mcard_array;
    $_SESSION['p_result'] = $p_result;
    $_SESSION['gameresult'] = $gameresult;
    echo json_encode (array ('status' => $status));
}
//---判斷爆牌function---
function overtake21 ($point)
{
    if (21 < $point)
    {
        $judge = ' 爆牌';
    }
    else
    {
        $judge = '';
    }
    return $judge;
}
//---判斷點數大小function---
function compare ($m_point, $p_point)
{
    $gameresult = '';
    $gameresult = ($m_point > $p_point) ? LOSE : $gameresult;
    $gameresult = ($m_point == $p_point) ? TIE : $gameresult;
    $gameresult = ($m_point < $p_point) ? WIN : $gameresult;

    return $gameresult;
}
function compareToString ($result)
{
    $toString = "Error";
    $toString = (LOSE == $result) ? "比莊家小，輸了" : $gameresult;
    $toString = (TIE == $result) ? "點數一樣，平手" : $gameresult;
    $toString = (WIN == $result) ? "比莊家大，勝利" : $gameresult;

    return $toString;
}
