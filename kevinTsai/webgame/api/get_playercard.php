<?php
require ('config.php');
include ('../search_function.php');

$id = $_POST['id'];
//$id = '1';
//---判斷是否為數字---
if (!is_numeric ($id))
{
    $status = -87;
    echo json_encode (array ('status' => $status));
}
else
{
    //---呼叫查詢玩家手牌function
    $result = select_pcard ($id);

    //---查詢成功，回傳牌代號
    if ($result)
    {
        $status = 1;
        //---轉換資料型態str->array
        $pcard[] = explode (",", $result['p_card']);
        
        echo json_encode (array ('status' => $status, 'p_card' => $pcard));
    }
    else
    {
        $status = -87;
        echo json_encode (array ('status' => $status));
    }
}

