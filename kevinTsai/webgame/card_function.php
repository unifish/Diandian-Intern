<?php 

//---方法：洗牌---
function shuffling ($card)
{
    //---將牌隨機分成三份
    do
    {
        $box[rand(0, 2)][] = array_shift ($card);
    } while (count ($card) != 0);

    //---每份洗牌
    shuffle ($box[0]);
    shuffle ($box[1]);
    shuffle ($box[2]);

    //---一張一張放回
    do
    {
        $pokers[] = array_shift ($box[0]);
    } while (count ($box[0]) != 0);

    do
    {
        $pokers[] = array_shift ($box[1]);
    } while (count ($box[1]) != 0);
    
    do
    {
        $pokers[] = array_shift ($box[2]);
    } while (count ($box[2]) != 0);

    return $pokers;
}
//---方法：補牌---
function add_card ()
{
    global $cards;   
    if (0 != count ($cards))
    {
        $take_card = array_shift ($cards);  //取出牌組第一張
        return $take_card;
    } 
    else 
    {
        return $take_card;
    }
}
//---方法：檢查玩家手牌---
function check_playercard ($card_str)
{
    $status = 1;
    //---轉換資料型態str->array
    $card_array = explode (",", $card_str);
    foreach ($card_array as $key => $info)
    {
        $p_card[] = explode ("-", $info);
    }
    //---呼叫計算點數function
    $p_sum = point_compute ($p_card);

    //---判斷爆牌或是滿牌
    if (21 < $p_sum )
    {
        $status = 21;
    }
    else if (5 == count ($p_card))
    {
        $status = 5;
    }
    return $status;
}
//---方法：計算點數---
function point_compute ($get_card)
{
    $sum = 0;
    $have_A = 0;
    foreach ($get_card as $card => $cardpoint)
    {
        $get_point = $cardpoint[1];
        //---點數為11、12、13時，視為10點
        if (11 == $get_point or 12 == $get_point or 13 == $get_point)
        {
            $sum += 10;
        }
        else if (1 == $get_point)
        {
            $have_A += 1;
        }
        else
        {
        $sum += $get_point;
        }
    }
    //---A視為1或11
    for ($i = 0; $i < $have_A; $i++)
    {
        $A_point = (($sum + 11) <= 21 && 1 == ($have_A - $i)) ? 11 : 1;
        $sum += $A_point;
    }
    return $sum;
}
//---方法：手上是否有A---
function have_A ($get_player)
{
    global $p_card;
    $have_A = false;
    foreach ($p_card[$get_player] as $card => $cardpoint)
    {
        $get_point = $cardpoint[1];
        if (1 == $get_point)
        {
            $have_A = true;
        }
    }
    return $have_A;
}

