<?php

class Card {
	public $color;
	public $point;

	public $color_name;
	public $point_name;

	public $pic;

	public function __construct($color, $point, $color_name, $point_name) {
		$this->color = $color;
		$this->point = $point;

		$this->color_name = $color_name;
		$this->point_name = $point_name;
		$this->_genPic($color, $point);
	}

	private function _genPic($color, $point) {
		$color_map = array(NULL, 'clubs', 'diamonds', 'hearts', 'spades');
		$point_map = array(NULL, 'ace', '2', '3', '4', '5', '6', '7',
						   '8', '9', '10', 'jack', 'queen', 'king');
		$this->pic = 'http://raguhn.raguhn.tw/intern/poker_cards/'.$point_map[$point].'_of_'.$color_map[$color].'.png';
	}
}