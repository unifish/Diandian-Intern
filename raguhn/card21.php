<?php
include('./card.php');
//輸入
$player_quantity = $argv[1];
$card_quantity = $argv[2];

if (!is_numeric($player_quantity) || !is_numeric($card_quantity)) {
	echo 'PARAMETER ERROR'."\n";
	exit;
}

//定義
$cards = array();
$colors = array(NULL, '梅花', '方塊', '紅心', '黑桃');
$points = array(NULL, 'A', '2', '3', '4', '5', '6',
				 '7', '8', '9','10', 'J', 'Q', 'K');
unset($colors[0]);
unset($points[0]);
//建牌組
foreach ($colors as $color_key => $color_name) {
	foreach ($points as $point_key => $point_name) {
		$cards[] = new Card($color_key, $point_key, $color_name, $point_name);
	}
}

//洗牌
//完美洗牌50次 + 普通洗牌 1000次
for ($i = 0; $i < 1000; ++$i) {
	$cards = wash_normal($cards);
	if (0 == ($i % 20)) {
		$cards = wash_perfect($cards);
	}
}

//發牌
$players = array();
for ($i = 0; $i < $card_quantity; ++$i) {
	for ($j = 0; $j < $player_quantity; ++$j) {
		if (0 === count($cards)) {
			break 2;
		}
		$player = array();
		$players[$j][] = array_shift($cards);
	}
}

//輸出
foreach ($players as $key => $player) {
	//組card字串
	$card_string = '';
	foreach ($player as $card) {
		$full_card = $card->color_name . $card->point_name;
		$card_string.= '' == $card_string ? $full_card : ', '.$full_card;
	}
	echo '玩家'.strval($key + 1).': '.$card_string."\n";
}
function wash_normal($cards) {
	//把牌從中間取一些出來，模擬人的不精準
	$start = rand(14, 20);
	$quantity = rand(14, 20);
	$card_hash = array_splice($cards, $start, $quantity);
	//index重排
	array_values($cards);
	array_values($card_hash);
	$cards = array_merge($card_hash, $cards);
	return $cards;
}

function wash_perfect($cards) {
	//牌分成兩堆，模擬人的不精準
	$quantity = rand(20, 30);
	$card_hash_1 = array_splice($cards, 0, $quantity);
	$card_hash_2 = $cards;
	//index重排
	array_values($card_hash_1);
	array_values($card_hash_2);
	$cards = array();
	do {
		if (isset($card_hash_1[0])) {
			$cards[] = array_shift($card_hash_1);
			array_values($card_hash_1);
		}
		if (isset($card_hash_2[0])) {
			$cards[] = array_shift($card_hash_2);
			array_values($card_hash_2);
		}
	} while (count($cards) < 52);
	return $cards;
}