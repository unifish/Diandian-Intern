<?php

//TODO: 創建牌組
$cards = array();

//建立'牌'
//color: 3 => 黑桃
//		 2 => 紅心
//		 1 => 方塊
//		 0 => 梅花
//point: A => 1
//        ....
//        ..
//        .
//       K => 13

for ($i = 0; $i<4; ++$i) {
	//產生花色
	for ($j = 1; $j <= 13; ++$j) {
		//產生點數
		$card = array(
			'color' => $i,
			'point' => $j,
			);
		$cards[] = $card;
	}
}

//TODO: 洗牌

shuffle($cards);

//TODO: 輸出
$colors = array('梅花', '方塊', '紅心', '黑桃');
$points = array('', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K');

while (count($cards) > 0) {
	$card = array_shift($cards);
	$color = $card['color'];
	$point = $card['point'];
	echo $colors[$color].$points[$point]."\n";
	echo count($cards)."\n";
}